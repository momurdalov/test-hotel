import styles from "./HotelItem.module.scss";
import Rating from "@material-ui/lab/Rating";
import { useState } from "react";
import Vector from "../../../assets/images/Vector.png";

export type IHotelItemProps = {
  name: string;
  rating: number;
  reviewCount: number;
  description: string;
  price: number;
  button?: boolean;
};

const HotelItem: React.FC<IHotelItemProps> = ({
  name,
  rating,
  reviewCount,
  description,
  price,
  button,
}) => {
  const [active, setActive] = useState(true);

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.name}>{name}</div>
        <div className={styles.ratingCont}>
          <div className={styles.rating}>
            <Rating
              name="mobile-rating"
              className={`mobile ${styles.rating}`}
              size="small"
              value={rating}
            />
          </div>
          <div className={styles.reviewCount}>{reviewCount} отзыва</div>
        </div>

        <div className={styles.description}>{description}</div>
        <div className={styles.btnPriceCont}>
          <button
            className={active ? styles.button : styles.buttonActive}
            onClick={() => setActive(!active)}
          >
            {active ? (
              <div>
                <img src={Vector} alt="" className={styles.img} />
                Забронировать
              </div>
            ) : (
              <div>Забронировано</div>
            )}
          </button>
          <div className={styles.priceCont}>
            <span className={styles.price}>{price} ₽</span>
            <span className={styles.priceNight}>Цена за 1 ночь</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HotelItem;
