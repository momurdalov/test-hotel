import HotelItem, { IHotelItemProps } from "./HotelItem/HotelItem";
import styles from "./HotelList.module.scss";

interface IArrayProps {
  list?: IHotelItemProps[];
}

const HotelList: React.FC<IArrayProps> = ({ list }) => {
  return (
    <div className={styles.container}>
      {list?.map((o, i) => (
        <HotelItem key={i} {...o} />
      ))}
    </div>
  );
};

export default HotelList;
