import React, { useRef, useState } from "react";
import styles from "./MultiplySelect.module.scss";
import VectorDown from "../../assets/images/VectorDown.png";
import VectorUp from "../../assets/images/VectorUp.png";

interface IMultiplySelectProps {
  value?: number[];
  onChange: (value: number[]) => void;
}

const options = [1, 2, 3, 4, 5];

const MultiplySelect: React.FC<IMultiplySelectProps> = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const d = useRef<HTMLDivElement>(null);

  return (
    <div>
      <div
        className={styles.topCont}
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <div className={styles.starsText}>Количество звезд</div>

        <div className={styles.starsCont}>
          <div className={styles.starsInput}>
            {props.value && props.value.length > 0 ? (
              <div className={styles.selectedCont}>
                <div className={styles.selectedStars}>
                  {props.value.map((item) => (
                    <div key={item} className={styles.selectedStar}>{item} звезда</div>
                  ))}
                </div>
              </div>
            ) : (
              <div className={styles.starsText2}>Нажмите, для выбора звезд</div>
            )}
            {isOpen ? (
              <img src={VectorUp} alt="" className={styles.imgVector} />
            ) : (
              <img src={VectorDown} alt="" className={styles.imgVector} />
            )}
          </div>
        </div>
      </div>
      <div
        ref={d}
        style={{ maxHeight: isOpen ? 235 + "px" : "0px" }}
        className={styles.optionsCont}
      >
        <div className={styles.optionsContItem}>
          {options.map((option) => (
            <div key={option} className={styles.optionsItem}>
              <input
                className={styles.checkboxItem}
                type="checkbox"
                onChange={(e) => {
                  if (e.currentTarget.checked) {
                    props.onChange([...(props.value ?? []), option]);
                  } else {
                    props.onChange(
                      props.value
                        ? props.value.filter((value) => value !== option)
                        : []
                    );
                  }
                }}
                value={props.value?.find((x) => x === option)}
              />
              {option} звезда
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MultiplySelect;
