import { useEffect, useState } from "react";
import MultiRangeSlider from "./MultiRangeSlider/MultiRangeSlider";
import styles from "./RangeInput.module.scss";

interface IRangeInputProps {
  min?: number;
  max: number;
  onChange: (min: number, max: number) => void;
}

const RangeInput: React.FC<IRangeInputProps> = (props) => {
  const [minVal, setMinVal] = useState(props.min ?? 0);
  const [maxVal, setMaxVal] = useState(props.max);

  useEffect(() => {
    props.onChange(minVal, maxVal);
  }, [minVal, maxVal, props]);

  return (
    <div>
      <div className={styles.container}>
        <div className={styles.priceText}>Цена</div>
        <div className={styles.inputsCont}>
          <input
            min={props.min}
            max={maxVal}
            value={minVal}
            type="number"
            className={styles.inputFirst}
            onChange={(e) => {
              const _minVal = Number(e.currentTarget.value);
              if (_minVal > maxVal) setMaxVal(Number(e.currentTarget.value));
              else return;
            }}
          />
          <span className={styles.tire}>-</span>
          <input
            min={minVal}
            value={maxVal}
            max={props.max}
            type="number"
            className={styles.inputFirst}
            onChange={(e) => {
              const _maxVal = Number(e.currentTarget.value);
              if (_maxVal > minVal) setMaxVal(Number(e.currentTarget.value));
              else return;
            }}
          />
        </div>
      </div>
      <MultiRangeSlider
        min={props.min}
        max={props.max}
        minVal={minVal}
        maxVal={maxVal}
        setMinVal={setMinVal}
        setMaxVal={setMaxVal}
        onChange={(e, r) => {}}
      />
    </div>
  );
};

export default RangeInput;
