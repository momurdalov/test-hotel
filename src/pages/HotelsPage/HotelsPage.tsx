import { useCallback, useEffect, useMemo, useState } from "react";
import { IHotelItemProps } from "../../components/HotelList/HotelItem/HotelItem";
import HotelList from "../../components/HotelList/HotelList";
import MultiplySelect from "../../components/MultiplySelect/MultiplySelect";
import RangeInput from "../../components/RangeInput/RangeInput";
import styles from "./HotelsPage.module.scss";

interface IStageHotelItemProps {
  list: IHotelItemProps[];
}

const HotelsPage: React.FC<IStageHotelItemProps> = ({ list }) => {
  const [selectedStars, setSelectedStars] = useState<number[]>();
  const [minCountReview, setMinCountReview] = useState<number>();
  const [needFilter, setNeedFilter] = useState(false);

  const maxPrice = useMemo(() => {
    const prices = list.map((x) => x.price);
    return Math.max(...prices);
  }, [list]);
  const [minPriceVal, setMinPriceVal] = useState(0);
  const [maxPriceVal, setMaxPriceVal] = useState(maxPrice);

  const [filteredList, setFilteredList] = useState(list);

  const updateList = useCallback(() => {
    let _list = list;

    if (needFilter) {
      if (selectedStars)
        _list = _list.filter((x) => selectedStars.includes(x.rating));
      if (minPriceVal) _list = _list.filter((x) => x.price > minPriceVal);
      if (maxPriceVal) _list = _list.filter((x) => x.price <= maxPriceVal);
      if (minCountReview)
        _list = _list.filter((x) => x.reviewCount >= minCountReview);
    }
    setFilteredList(_list);
  }, [
    list,
    needFilter,
    minPriceVal,
    maxPriceVal,
    minCountReview,
    selectedStars,
  ]);

  useEffect(() => {
    updateList();
  }, [
    list,
    needFilter,
    minPriceVal,
    maxPriceVal,
    minCountReview,
    selectedStars,
    updateList,
  ]);

  return (
    <div>
      <div>
        <MultiplySelect value={selectedStars} onChange={setSelectedStars} />
        <div className={styles.reviewCont}>
          <div className={styles.reviewsText}>Количество отзывов (от)</div>
          <input
            type="number"
            placeholder="Например, от 10"
            value={minCountReview}
            className={styles.reviewCountCont}
            onChange={(e) => {
              const value = e.currentTarget.value;
              setMinCountReview(value ? Number(value) : undefined);
            }}
          />
        </div>
        <RangeInput
          max={maxPrice}
          onChange={(min, max) => {
            setMinPriceVal(min);
            setMaxPriceVal(max);
          }}
        />
        <div className={styles.btnsCont}>
          <button
            onClick={() => setNeedFilter(true)}
            className={styles.filterBtn}
          >
            Применить фильтр
          </button>
          {needFilter && (
            <button
              onClick={() => setNeedFilter(false)}
              className={styles.filterClearBtn}
            >
              <span className={styles.iks}>×</span> Очистить фильтр
            </button>
          )}
        </div>

        <HotelList list={filteredList} />
      </div>
    </div>
  );
};

export default HotelsPage;
