import { IHotelItemProps } from "../components/HotelList/HotelItem/HotelItem";

export const Items: IHotelItemProps[] = [
  {
    name: "Marina In",
    rating: 3,
    reviewCount: 3,
    description:
      "Этот 4-звездочный отель расположен в центре города. На его территории есть бассейн с терассой и сауна. Из номеров открывается вид на море.",
    price: 44600,
  },
  {
    name: "Sahil Park Hotel",
    rating: 5,
    reviewCount: 1,

    description:
      "Этот 4-звездочный отель расположен в центре города. На его территории есть бассейн с терассой и сауна. Из номеров открывается вид на море.",
    price: 3700,
  },
  {
    name: "Sunny Appartmants",
    rating: 5,
    reviewCount: 23,

    description:
      "Этот 4-звездочный отель расположен в центре города. На его территории есть бассейн с терассой и сауна. Из номеров открывается вид на море.",
    price: 2800,
  },
  {
    name: "Mondrian Suites",
    rating: 5,
    reviewCount: 23,
    description:
      "Этот 4-звездочный отель расположен в центре города. На его территории есть бассейн с терассой и сауна. Из номеров открывается вид на море.",
    price: 5450,
  },
];
