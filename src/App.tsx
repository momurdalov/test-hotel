import React from "react";
import "./App.css";
import { Items } from "./pages/Data";
import HotelsPage from "./pages/HotelsPage/HotelsPage";

function App() {
  return (
    <div className="App">
      <HotelsPage list={Items} />
    </div>
  );
}

export default App;
